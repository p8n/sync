use failure::ResultExt;
use memmap::MmapOptions;
use rocksdb;
use ron_crdt::{Atom, CRDT, LWW, UUID};
use types::Region;

use crate::constants::*;
use crate::utils::*;
use crate::{Database, Error, Result};

/*
Region: LWW {
  name: &Set<String>
  type: String,
  offset: Integer
  sha256: String,
  bits: Integer,
}

Literal: Region {
  data: String,
}

External: Region {
  url: String
  foffset: Integer
  size: Integer
}
*/

pub fn serialize_region(r: &Region, db: &mut Database) -> Result<()> {
    use base64;

    let frm = LWW::new(*r.uuid());
    let def = r.defined();
    let len = def.end - def.start;

    db.merge(
        *r.uuid(),
        &LWW::set(&frm, *NAME_UUID, Atom::String(r.name().to_string()))
            .unwrap(),
    )?;
    db.merge(
        *r.uuid(),
        &LWW::set(&frm, *OFFSET_UUID, Atom::Integer(def.start as i64)).unwrap(),
    )?;
    db.merge(
        *r.uuid(),
        &LWW::set(&frm, *SIZE_UUID, Atom::Integer(len as i64)).unwrap(),
    )?;
    db.merge(
        *r.uuid(),
        &LWW::set(&frm, *BITS_UUID, Atom::Integer(r.address_bits() as i64))
            .unwrap(),
    )?;

    match r.file() {
        Some((p, off)) => {
            let path = format!("{}", p.display());

            db.merge(
                *r.uuid(),
                &LWW::set(&frm, *TYPE_UUID, Atom::UUID(*EXT_UUID)).unwrap(),
            )?;
            db.merge(
                *r.uuid(),
                &LWW::set(&frm, *URL_UUID, Atom::String(path)).unwrap(),
            )?;
            db.merge(
                *r.uuid(),
                &LWW::set(&frm, *FOFF_UUID, Atom::Integer(off as i64)).unwrap(),
            )?;
        }

        None => {
            let data = if def.start == def.end {
                "".to_string()
            } else {
                base64::encode(r.read(def.start, len as usize).unwrap())
            };

            db.merge(
                *r.uuid(),
                &LWW::set(&frm, *TYPE_UUID, Atom::UUID(*LIT_UUID)).unwrap(),
            )?;
            db.merge(
                *r.uuid(),
                &LWW::set(&frm, *DATA_UUID, Atom::String(data)).unwrap(),
            )?;
        }
    }

    Ok(())
}

impl Database {
    pub(crate) fn deserialize_region<'a>(
        uuid: UUID, db: &rocksdb::DB,
    ) -> Result<Region> {
        use std::fs::File;
        use std::path::PathBuf;

        let region = Self::get_impl(db, uuid, *LWW_UUID)?;
        let region = LWW::map(region).unwrap();

        let name = string_field(*NAME_UUID, &region)
            .map(|s| s.to_string())
            .unwrap_or("(no name)".into());
        let typ = uuid_field(*TYPE_UUID, &region)?;
        let offset = int_field(*OFFSET_UUID, &region).map(|i| i as u64)?;
        let bits = int_field(*BITS_UUID, &region).map(|i| i as usize)?;

        if typ == *LIT_UUID {
            let data = string_field(*DATA_UUID, &region)?;

            if data.is_empty() {
                Ok(Region::undefined(name, bits, uuid))
            } else {
                Ok(Region::from_buf(
                    name,
                    bits,
                    base64::decode(data)?,
                    offset,
                    uuid,
                ))
            }
        } else if typ == *EXT_UUID {
            let url = string_field(*URL_UUID, &region).unwrap();
            let foff = int_field(*FOFF_UUID, &region).map(|i| i as u64)?;
            let size = int_field(*SIZE_UUID, &region).map(|i| i as u64)?;

            let fd = File::open(url)
                .context(format!("open file backing region {}", uuid))?;
            let mmap = unsafe {
                MmapOptions::new()
                    .len(size as usize)
                    .offset(foff as usize)
                    .map(&fd)
                    .unwrap()
            };

            Ok(Region::from_mmap(
                name,
                bits,
                mmap,
                PathBuf::from(url),
                foff,
                offset,
                uuid,
            ))
        } else {
            Err(Error::from(format!("unknown region type {:?}", typ)).into())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn undefined() {
        let l1 = Region::undefined("l1", 6, None);
        let mut db = Database::ephemeral(vec![l1.clone()]).unwrap();

        serialize_region(&l1, &mut db).unwrap();

        let l2 = Database::deserialize_region(*l1.uuid(), &db.db).unwrap();

        assert_eq!(l1.defined(), l2.defined());
        assert_eq!(l1.name(), l2.name());
        assert_eq!(l1.uuid(), l2.uuid());
        assert_eq!(l1.address_bits(), l2.address_bits());
    }

    #[test]
    fn from_buf() {
        let b: Box<[u8]> = vec![1, 2, 3].into();
        let l1 = Region::from_buf("l2", 16, b, 5, None);
        let mut db = Database::ephemeral(vec![l1.clone()]).unwrap();

        serialize_region(&l1, &mut db).unwrap();

        let l2 = Database::deserialize_region(*l1.uuid(), &db.db).unwrap();

        assert_eq!(l1.defined(), l2.defined());
        assert_eq!(l1.name(), l2.name());
        assert_eq!(l1.uuid(), l2.uuid());
        assert_eq!(l1.address_bits(), l2.address_bits());
        assert_eq!(l1.read(5, 3).ok(), l2.read(5, 3).ok());
    }

    #[test]
    fn from_file() {
        use std::io::Write;
        use tempfile::NamedTempFile;

        let mut file = NamedTempFile::new().unwrap();
        file.write_all(b"\x01\x02\x03").unwrap();

        let l1 = Region::from_file(
            "l1",
            16,
            file.reopen().unwrap(),
            file.path().to_path_buf(),
            5,
            None,
        )
        .unwrap();
        let mut db = Database::ephemeral(vec![l1.clone()]).unwrap();

        serialize_region(&l1, &mut db).unwrap();

        let l2 = Database::deserialize_region(*l1.uuid(), &db.db).unwrap();

        assert_eq!(l1.defined(), l2.defined());
        assert_eq!(l1.name(), l2.name());
        assert_eq!(l1.uuid(), l2.uuid());
        assert_eq!(l1.address_bits(), l2.address_bits());
        assert_eq!(l1.read(5, 3).ok(), l2.read(5, 3).ok());
    }

    #[test]
    fn from_missing_file() {
        use std::io::Write;
        use tempfile::NamedTempFile;

        let l1 = {
            let mut file = NamedTempFile::new().unwrap();
            file.write_all(b"\x01\x02\x03").unwrap();

            Region::from_file(
                "l1",
                16,
                file.reopen().unwrap(),
                file.path().to_path_buf(),
                5,
                None,
            )
            .unwrap()
        };
        let mut db = Database::ephemeral(vec![l1.clone()]).unwrap();

        serialize_region(&l1, &mut db).unwrap();

        assert!(Database::deserialize_region(*l1.uuid(), &db.db).is_err());
    }
}

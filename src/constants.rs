use ron_crdt::UUID;
use std::str::FromStr;

lazy_static! {
    pub static ref BITS_UUID: UUID = UUID::from_str("bits").unwrap();
    pub static ref BLOCK_UUID: UUID = UUID::from_str("block").unwrap();
    pub static ref DATA_UUID: UUID = UUID::from_str("data").unwrap();
    pub static ref DB_UUID: UUID = UUID::from_str("db").unwrap();
    pub static ref ENTRY_UUID: UUID = UUID::from_str("entrypoint").unwrap();
    pub static ref EXT_UUID: UUID = UUID::from_str("external").unwrap();
    pub static ref FNS_UUID: UUID = UUID::from_str("functions").unwrap();
    pub static ref FOFF_UUID: UUID = UUID::from_str("foffset").unwrap();
    pub static ref FUNCTION_UUID: UUID = UUID::from_str("function").unwrap();
    pub static ref GUARD_UUID: UUID = UUID::from_str("guard").unwrap();
    pub static ref LIT_UUID: UUID = UUID::from_str("literal").unwrap();
    pub static ref LWW_UUID: UUID = UUID::from_str("lww").unwrap();
    pub static ref MARKERS_UUID: UUID = UUID::from_str("markers").unwrap();
    pub static ref NAME_UUID: UUID = UUID::from_str("name").unwrap();
    pub static ref NEXT_UUID: UUID = UUID::from_str("next").unwrap();
    pub static ref OFFSET_UUID: UUID = UUID::from_str("offset").unwrap();
    pub static ref REGION_UUID: UUID = UUID::from_str("region").unwrap();
    pub static ref REGS_UUID: UUID = UUID::from_str("regions").unwrap();
    pub static ref SET_UUID: UUID = UUID::from_str("set").unwrap();
    pub static ref SHA256_UUID: UUID = UUID::from_str("sha256").unwrap();
    pub static ref SIZE_UUID: UUID = UUID::from_str("size").unwrap();
    pub static ref START_UUID: UUID = UUID::from_str("start").unwrap();
    pub static ref STOP_UUID: UUID = UUID::from_str("stop").unwrap();
    pub static ref TARGET_UUID: UUID = UUID::from_str("target").unwrap();
    pub static ref TYPE_UUID: UUID = UUID::from_str("type").unwrap();
    pub static ref URL_UUID: UUID = UUID::from_str("url").unwrap();
}

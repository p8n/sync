use std::convert::From;
use std::result;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "general error: {}", message)]
    General { message: String },
    #[fail(display = "missing field {} while deserializing {}", field, typ)]
    MissingField { field: &'static str, typ: &'static str },
}

impl From<&str> for Error {
    fn from(s: &str) -> Error {
        Error::General { message: s.to_string() }
    }
}

impl From<String> for Error {
    fn from(s: String) -> Error {
        let s: &str = &s;
        Error::from(s)
    }
}

pub type Result<T> = result::Result<T, failure::Error>;

use std::collections::{HashMap, HashSet};

use futures::{future, Future};
use petgraph::visit::EdgeRef;
use petgraph::Direction;
use rocksdb;
use ron_crdt::{Atom, Set, CRDT, LWW, UUID};
use smallvec::SmallVec;
use types::{
    CfgNode, Constant, Function, Guard, Names, Region, Value, Variable,
};

use crate::constants::*;
use crate::utils::*;
use crate::{Database, Error, Result};

macro_rules! pry {
    ($e:expr) => {
        match $e {
            Ok(x) => x,
            Err(e) => {
                return (vec![], Box::new(future::err(e)));
            }
        }
    };
}

pub fn serialize_function(f: &Function, db: &mut Database) -> Result<()> {
    let frm = LWW::new(*f.uuid());

    for (_, bb) in f.basic_blocks() {
        // BasicBlock: Marker
        let nx_uu = UUID::now();
        let nx_frm = Set::new(nx_uu);
        let frm = LWW::new(bb.uuid);

        db.merge(bb.uuid, &frm)?;
        db.merge(
            bb.uuid,
            &LWW::set(&frm, *START_UUID, Atom::Integer(bb.area.start as i64))
                .unwrap(),
        )?;
        db.merge(
            bb.uuid,
            &LWW::set(&frm, *STOP_UUID, Atom::Integer(bb.area.end as i64))
                .unwrap(),
        )?;
        db.merge(
            bb.uuid,
            &LWW::set(&frm, *REGION_UUID, Atom::UUID(*f.region())).unwrap(),
        )?;
        db.merge(
            bb.uuid,
            &LWW::set(&frm, *TYPE_UUID, Atom::UUID(*BLOCK_UUID)).unwrap(),
        )?;
        db.merge(
            bb.uuid,
            &LWW::set(&frm, *FUNCTION_UUID, Atom::UUID(*f.uuid())).unwrap(),
        )?;
        db.merge(
            bb.uuid,
            &LWW::set(&frm, *NEXT_UUID, Atom::UUID(nx_uu)).unwrap(),
        )?;

        db.merge(nx_uu, &nx_frm)?;

        // Jump: LWW
        let cfg = f.cflow_graph();
        for e in cfg.edges_directed(bb.node, Direction::Outgoing) {
            let uu = UUID::now();
            let frm = LWW::new(uu);
            let tgt = match cfg.node_weight(e.target()) {
                Some(&CfgNode::BasicBlock(idx)) => {
                    Atom::UUID(f.basic_block(idx).uuid)
                }
                Some(&CfgNode::Value(Value::Variable(Variable {
                    name,
                    ..
                }))) => {
                    Atom::String(
                        f.names.value(name).unwrap().base().to_string(),
                    )
                }
                Some(&CfgNode::Value(Value::Constant(Constant {
                    value,
                    ..
                }))) => Atom::Integer(value as i64),
                _ => {
                    continue;
                }
            };
            let guard = Atom::String(format!("{:?}", e.weight()));

            db.merge(uu, &frm)?;
            db.merge(uu, &LWW::set(&frm, *TARGET_UUID, tgt).unwrap())?;
            db.merge(uu, &LWW::set(&frm, *GUARD_UUID, guard).unwrap())?;

            db.merge(nx_uu, &Set::insert(&nx_frm, Atom::UUID(uu)).unwrap())?;
        }
    }

    let entry = f.basic_block(f.entry_point());

    db.merge(
        *f.uuid(),
        &LWW::set(&frm, *NAME_UUID, Atom::String(f.name.to_string())).unwrap(),
    )?;
    db.merge(
        *f.uuid(),
        &LWW::set(&frm, *ENTRY_UUID, Atom::UUID(entry.uuid)).unwrap(),
    )?;

    Ok(())
}

impl Database {
    pub(crate) fn deserialize_function<'a>(
        uuid: UUID, regions: &[Region], db: &rocksdb::DB,
    ) -> (Vec<UUID>, Box<Future<Item = Function, Error = failure::Error>>) {
        use std::collections::HashSet;
        use std::ops::Range;

        let func = pry!(Self::get_impl(db, uuid, *LWW_UUID).and_then(|func| {
            LWW::map(func).ok_or(Error::from("failed to map function").into())
        }));

        let mut refd = Vec::default();
        let name = pry!(string_field(*NAME_UUID, &func));
        let entry = pry!(uuid_field(*ENTRY_UUID, &func));
        let mut blocks =
            Vec::<(UUID, Range<u64>, SmallVec<[(Atom, Guard); 2]>)>::default();
        let mut entry_point = None;
        let mut region = None;
        let mut seen = HashSet::<UUID>::default();
        let mut todo = HashSet::<UUID>::default();
        let mut starts = HashMap::<UUID, u64>::default();
        let names = Names::default();

        todo.insert(entry);

        while let Some(uu) = todo.iter().next().cloned() {
            let bb = pry!(Self::get_impl(db, uu, *LWW_UUID).and_then(|bb| {
                LWW::map(bb)
                    .ok_or(Error::from("failed to map basic block").into())
            }));
            let start = pry!(int_field(*START_UUID, &bb)) as u64;
            let stop = pry!(int_field(*STOP_UUID, &bb)) as u64;
            let reg_uuid = pry!(uuid_field(*REGION_UUID, &bb));
            let typ = pry!(uuid_field(*TYPE_UUID, &bb));
            let reg =
                regions.iter().find(|reg| reg.uuid() == &reg_uuid).cloned();

            if typ == *BLOCK_UUID {
                let this_func = pry!(uuid_field(*FUNCTION_UUID, &bb));

                if this_func == uuid {
                    let next = pry!(uuid_field(*NEXT_UUID, &bb));
                    let next_pairs = pry!(Self::deserialize_branch(
                        next, db, &mut todo, &mut seen, &mut refd,
                    ));

                    blocks.push((uu.clone(), start..stop, next_pairs));
                    starts.insert(uu.clone(), start);

                    if entry_point.is_none() {
                        entry_point = Some(start);
                    }

                    if region.is_none() && reg.is_some() {
                        region = reg;
                    }
                }
            }

            todo.remove(&uu);
            seen.insert(uu);
        }

        refd.extend(seen.into_iter());

        let blocks = blocks
            .into_iter()
            .map(|(uu, rgn, jmps)| {
                let jmps = jmps
                    .into_iter()
                    .filter_map(|(atm, gu)| {
                        match atm {
                            Atom::UUID(ref uu) => {
                                Value::val(starts[uu], 64).ok()
                            }
                            Atom::Integer(i) => Value::val(i as u64, 64).ok(),
                            Atom::Float(_) | Atom::String(_) => None,
                        }
                        .map(|v| (v, gu))
                    })
                    .collect();
                (uu, rgn, jmps)
            })
            .collect();
        let name = name.to_string();
        let fut = Box::new(future::lazy(move || {
            let reg = region.unwrap();
            let f = Function::known_cflow_graph::<types::TestArch>(
                (),
                names,
                blocks,
                entry_point.unwrap(),
                &reg,
                uuid,
                name,
            )
            .map_err(|e| Error::from(format!("p8n: {}", e)).into());

            future::result(f)
        }));

        (refd, fut)
    }

    fn deserialize_branch<'a>(
        uuid: UUID, db: &rocksdb::DB, todo: &mut HashSet<UUID>,
        seen: &mut HashSet<UUID>, refd: &mut Vec<UUID>,
    ) -> Result<SmallVec<[(Atom, Guard); 2]>> {
        Self::get_impl(db, uuid, *SET_UUID)
            .and_then(|next| {
                Set::map(next).ok_or(Error::from("failed to map branch").into())
            })?
            .into_iter()
            .filter_map(|atm| {
                match atm {
                    Atom::UUID(jmp) => {
                        refd.push(jmp);
                        Self::get_impl(db, jmp, *LWW_UUID)
                            .map(|jmp| {
                                let jmp = LWW::map(jmp).unwrap();
                                let tgt = jmp.get(&*TARGET_UUID);
                                let _ = string_field(*GUARD_UUID, &jmp)?;
                                // XXX
                                let gu = Guard::True;

                                match tgt {
                                    Some(&Atom::UUID(ref uu))
                                        if !seen.contains(uu)
                                            && !todo.contains(uu) =>
                                    {
                                        todo.insert(uu.clone());
                                    }
                                    _ => {}
                                }

                                Ok((tgt.unwrap().clone(), gu))
                            })
                            .ok()
                    }
                    _ => None,
                }
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use types::{Region, TestArch};

    #[test]
    fn successful() {
        let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let uu = UUID::now();
        let mut db = Database::ephemeral(vec![reg.clone()]).unwrap();
        let func1 = Function::new::<TestArch>((), 0, &reg, uu).unwrap();

        serialize_function(&func1, &mut db).unwrap();

        let bb1a = func1
            .basic_blocks()
            .map(|(_, bb)| bb.area.clone())
            .collect::<Vec<_>>();
        let bb1m = func1
            .basic_blocks()
            .map(|(_, bb)| bb.mnemonics.clone())
            .collect::<Vec<_>>();
        let mne1 = func1
            .mnemonics(..)
            .map(|(_, bb)| bb.area.clone())
            .collect::<Vec<_>>();

        let func2 = Database::deserialize_function(uu, &[reg.clone()], &db.db)
            .1
            .wait()
            .unwrap();
        let bb2a = func2
            .basic_blocks()
            .map(|(_, bb)| bb.area.clone())
            .collect::<Vec<_>>();
        let bb2m = func2
            .basic_blocks()
            .map(|(_, bb)| bb.mnemonics.clone())
            .collect::<Vec<_>>();
        let mne2 = func2
            .mnemonics(..)
            .map(|(_, bb)| bb.area.clone())
            .collect::<Vec<_>>();

        assert_eq!(bb1a.len(), 4);
        assert_eq!(bb2a.len(), 4);
        assert_eq!(mne1.len(), 8);
        assert_eq!(mne2.len(), 8);
        assert_eq!(bb1a[0], bb2a[0]);
        assert_eq!(mne1[0..3], mne2[0..3]);
        assert_eq!(bb1a[3], bb2a[3]);
        assert_eq!(mne1[6..8], mne2[6..8]);

        let swapped = bb1a[1] == bb2a[2] && bb1a[2] == bb2a[1];
        let bb11 = func1
            .mnemonics(bb1m[1].clone())
            .map(|(_, m)| m.area.clone())
            .collect::<Vec<_>>();
        let bb12 = func1
            .mnemonics(bb1m[2].clone())
            .map(|(_, m)| m.area.clone())
            .collect::<Vec<_>>();
        let bb21 = func2
            .mnemonics(bb2m[1].clone())
            .map(|(_, m)| m.area.clone())
            .collect::<Vec<_>>();
        let bb22 = func2
            .mnemonics(bb2m[2].clone())
            .map(|(_, m)| m.area.clone())
            .collect::<Vec<_>>();

        if swapped {
            assert!(bb1a[2] == bb2a[1] && bb1a[2] == bb2a[1]);
            assert_eq!(bb11, bb22);
            assert_eq!(bb12, bb21);
        } else {
            assert!(bb1a[1] == bb2a[1] && bb1a[2] == bb2a[2]);
            assert_eq!(bb11, bb21);
            assert_eq!(bb12, bb22);
        }
    }

    #[test]
    fn recover_cfg_bad_entry() {
        let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let uu = UUID::now();
        let mut db = Database::ephemeral(vec![reg.clone()]).unwrap();
        let func1 = Function::new::<TestArch>((), 0, &reg, uu).unwrap();

        serialize_function(&func1, &mut db).unwrap();

        // set entry
        let frm = LWW::set(
            &Database::get_impl(&db.db, *func1.uuid(), *LWW_UUID).unwrap(),
            *ENTRY_UUID,
            Atom::UUID(UUID::now()),
        )
        .unwrap();
        Database::merge_impl(&db.db, *func1.uuid(), &frm).unwrap();

        assert!(Database::deserialize_function(uu, &[reg.clone()], &db.db)
            .1
            .wait()
            .is_err());
    }

    #[test]
    fn recover_cfg_indirect_jmp() {
        let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut names = Names::default();
        let a = names.var("a", None, 32).unwrap();
        let cfg = vec![(
            UUID::now(),
            0..3,
            SmallVec::from_vec(vec![(Value::Variable(a), Guard::True)]),
        )];
        let uu = UUID::now();
        let func1 = Function::known_cflow_graph::<TestArch>(
            (),
            Names::default(),
            cfg,
            0,
            &reg,
            uu,
            "test".to_string(),
        )
        .unwrap();
        let mut db = Database::ephemeral(vec![reg.clone()]).unwrap();
        serialize_function(&func1, &mut db).unwrap();

        assert_eq!(
            func1.basic_blocks().collect::<Vec<_>>(),
            Database::deserialize_function(uu, &[reg.clone()], &*db.db)
                .1
                .wait()
                .unwrap()
                .basic_blocks()
                .collect::<Vec<_>>()
        );
    }

    #[test]
    fn recover_cfg_invalid_boundary() {
        let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
        let reg = Region::from_buf("", 16, data, 0, None);
        let mut names = Names::default();
        let a = names.var("a", None, 32).unwrap();
        let bb_uu = UUID::now();
        let cfg = vec![(
            bb_uu,
            0..7,
            SmallVec::from_vec(vec![(Value::Variable(a), Guard::True)]),
        )];
        let uu = UUID::now();
        let mut db = Database::ephemeral(vec![reg.clone()]).unwrap();
        let func1 = Function::known_cflow_graph::<TestArch>(
            (),
            Names::default(),
            cfg,
            0,
            &reg,
            uu,
            "test".to_string(),
        )
        .unwrap();
        serialize_function(&func1, &mut db).unwrap();

        // set stop
        let frm = LWW::set(
            &Database::get_impl(&*db.db, bb_uu, *LWW_UUID).unwrap(),
            *STOP_UUID,
            Atom::Integer(5),
        )
        .unwrap();
        Database::merge_impl(&*db.db, bb_uu, &frm).unwrap();

        assert!(Database::deserialize_function(uu, &[reg.clone()], &*db.db)
            .1
            .wait()
            .is_err());
    }
}

use std::collections::HashMap;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::str;
use std::sync::Arc;

use futures::{stream, Stream};
use memmap::MmapOptions;
use rocksdb;
use ron_crdt::{Atom, Batch, Frame, Set, CRDT, LWW, UUID};
use tempdir::TempDir;
use types::{Function, Region};

use crate::constants::*;
use crate::function::serialize_function;
use crate::region::serialize_region;
use crate::utils::*;
use crate::{Error, Result};

/*
**Globals**
name: String
markers: Set<Marker>
functions: Set<Function>
regions: Set<Region>
*/

pub struct Database {
    pub(crate) db: Arc<rocksdb::DB>,
    tempdir: TempDir,
    name: String,
    regions: Vec<Region>,
    function_refs: HashMap<UUID, UUID>,
    region_refs: HashMap<UUID, UUID>,
    function_set: UUID,
    marker_set: UUID,
    region_set: UUID,
}

impl Drop for Database {
    fn drop(&mut self) {
        use std::mem::drop;

        // we need to drop the database before tempdir.
        drop(&mut self.db);
        drop(&mut self.tempdir);
        drop(&mut self.name);
        drop(&mut self.regions);
        drop(&mut self.function_set);
        drop(&mut self.marker_set);
        drop(&mut self.region_set);
        drop(&mut self.function_refs);
        drop(&mut self.region_refs);
    }
}

fn reduce_operation(
    _: &[u8], existing_val: Option<&[u8]>,
    operands: &mut rocksdb::MergeOperands,
) -> Option<Vec<u8>> {
    let res = || -> Result<Vec<u8>> {
        let mut ops = operands
            .filter_map(|op: &[u8]| {
                str::from_utf8(op).ok().map(|s| Frame::parse(s))
            })
            .collect::<Vec<Frame>>();
        let (state, ops) = match existing_val {
            Some(state) => {
                let state = Frame::parse(str::from_utf8(state)?);
                (state, ops)
            }

            None => {
                match ops.len() {
                    0 => {
                        return Ok(Vec::default());
                    }
                    1 => {
                        return Ok(ops[0].body().as_bytes().into());
                    }
                    _ => {
                        let fst = ops.remove(0);
                        (fst, ops)
                    }
                }
            }
        };

        match state.peek().map(|op| op.ty) {
            Some(t) if t == *LWW_UUID => {
                LWW::reduce(state, ops)
                    .ok_or(Error::from("reduce failed").into())
                    .map(|frm| frm.body().as_bytes().into())
            }
            Some(t) if t == *SET_UUID => {
                Set::reduce(state, ops)
                    .ok_or(Error::from("reduce failed").into())
                    .map(|frm| frm.body().as_bytes().into())
            }
            Some(_) => Err(Error::from("unknown type").into()),
            None => Err(Error::from("empty state frame").into()),
        }
    }();

    match res {
        Ok(v) => Some(v),
        Err(e) => {
            eprintln!("reduce: {}", e);
            None
        }
    }
}

impl Database {
    pub fn ephemeral(regions: Vec<Region>) -> Result<Self> {
        Self::create("ephemeral", regions)
    }

    pub fn create(name: &str, regions: Vec<Region>) -> Result<Self> {
        let (db, tempdir) = Self::new_database()?;
        let db = Arc::new(db);

        // functions
        let fns_uu = UUID::now();
        let fns_frm = Set::new(fns_uu);
        db.merge(&Self::uuid_as_key(fns_uu)[..], fns_frm.body().as_bytes())?;

        // regions
        let rgs_uu = UUID::now();
        let rgs_frm = Set::new(rgs_uu);
        db.merge(&Self::uuid_as_key(rgs_uu)[..], rgs_frm.body().as_bytes())?;

        // markers
        let mrks_uu = UUID::now();
        let mrks_frm = Set::new(mrks_uu);
        db.merge(&Self::uuid_as_key(mrks_uu)[..], mrks_frm.body().as_bytes())?;

        let mut db = Database {
            db: db,
            tempdir: tempdir,
            function_set: fns_uu,
            marker_set: mrks_uu,
            region_set: rgs_uu,
            name: name.into(),
            regions: regions,
            function_refs: HashMap::default(),
            region_refs: HashMap::default(),
        };

        db.setup()?;
        Ok(db)
    }

    pub fn open_file(
        path: &Path,
    ) -> Result<(Self, impl Stream<Item = Function, Error = failure::Error>)>
    {
        let fd = File::open(path)?;
        let buf = unsafe { MmapOptions::new().map(&fd)? };
        let batch = Batch::parse(str::from_utf8(buf.as_ref())?);

        Self::open(batch)
    }

    pub fn open<'a>(
        initial: Batch<'a>,
    ) -> Result<(Self, impl Stream<Item = Function, Error = failure::Error>)>
    {
        let (db, tempdir) = Self::new_database()?;
        let db = Arc::new(db);

        for frame in initial {
            match frame.peek() {
                Some(op) => {
                    db.merge(
                        &Self::uuid_as_key(op.object)[..],
                        &frame.body().as_bytes(),
                    )?;
                }
                None => {}
            }
        }

        // load db fields
        let db_frm = Self::get_impl(&*db, *DB_UUID, *LWW_UUID)?;
        let db_obj = LWW::map(db_frm.clone()).unwrap();
        let fns_uu = uuid_field(*FNS_UUID, &db_obj)?;
        let rgs_uu = uuid_field(*REGS_UUID, &db_obj)?;
        let mrks_uu = uuid_field(*MARKERS_UUID, &db_obj)?;
        let name = string_field(*NAME_UUID, &db_obj)?;
        let rgs_frm = Self::get_impl(&*db, rgs_uu, *SET_UUID)?;
        let mut ret = Database {
            tempdir: tempdir,
            db: db,
            regions: Vec::default(),
            name: name.into(),
            function_set: fns_uu,
            marker_set: mrks_uu,
            region_set: rgs_uu,
            function_refs: HashMap::default(),
            region_refs: HashMap::default(),
        };

        // deserialize regions
        let mut rgs_refs = HashMap::default();
        let rgs = Set::map(rgs_frm)
            .unwrap()
            .into_iter()
            .filter_map(|atm| {
                match atm {
                    Atom::UUID(uu) => {
                        rgs_refs.insert(uu, uu);
                        Self::deserialize_region(uu, &ret.db).ok()
                    }
                    _ => None,
                }
            })
            .collect::<Vec<_>>();
        ret.regions = rgs;
        ret.region_refs = rgs_refs;

        let fns_frm = ret.get(fns_uu, *SET_UUID)?;
        let mut fn_refs = HashMap::default();
        let fns = {
            let rgs = ret.regions.clone();
            let db = ret.db.clone();
            Set::map(fns_frm)
                .unwrap()
                .into_iter()
                .map(|atm| {
                    match atm {
                        Atom::UUID(uu) => {
                            let (refs, fut) =
                                Self::deserialize_function(uu, &rgs, &*db);

                            for vv in refs {
                                fn_refs.insert(vv, uu);
                            }
                            fn_refs.insert(uu, uu);

                            fut
                        }
                        _ => unreachable!(),
                    }
                })
                .collect::<Vec<_>>()
        };
        ret.function_refs = fn_refs;

        Ok((ret, stream::futures_unordered(fns)))
    }

    pub fn receive(
        &mut self, frame: Frame,
    ) -> Result<impl Stream<Item = Function, Error = failure::Error>> {
        match frame.peek() {
            Some(op) => {
                let obj = op.object;

                if obj == *DB_UUID {
                    eprintln!("changed db field");
                } else if let Some(fn_uu) = self.function_refs.get(&obj) {
                    eprintln!("changed function {:?}", fn_uu);
                } else if obj == self.function_set {
                    eprintln!("changed function set");
                } else if obj == self.region_set {
                    eprintln!("changed region set");
                } else if let Some(reg_uu) = self.region_refs.get(&obj) {
                    eprintln!("changed region {:?}", reg_uu);
                } else if obj == self.marker_set {
                    eprintln!("changed marker set");
                } else {
                    eprintln!("unknown object changed");
                }
            }

            None => {}
        }

        Ok(stream::empty())
    }

    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn regions(&self) -> &[Region] {
        &self.regions
    }

    pub fn update_function(&mut self, function: &Function) -> Result<()> {
        serialize_function(function, self)?;

        // add to function list
        let frm = Set::new(self.function_set);

        self.merge(self.function_set.clone(), &frm)?;
        self.merge(
            self.function_set.clone(),
            &Set::insert(&frm, Atom::UUID(function.uuid().clone())).unwrap(),
        )?;

        Ok(())
    }

    pub fn remove_function(&mut self, function: &Function) -> Result<()> {
        // remove from function list
        let frm = self.get(self.function_set, *SET_UUID)?;

        self.merge(
            self.function_set.clone(),
            &Set::remove(frm, &Atom::UUID(function.uuid().clone())).unwrap(),
        )?;

        // remove basic blocks
        let mrks_frm = self.get(self.marker_set, *SET_UUID)?;
        self.merge(self.marker_set.clone(), &mrks_frm)?;

        for bb in function.basic_blocks() {
            self.merge(
                self.marker_set.clone(),
                &Set::remove(mrks_frm.clone(), &Atom::UUID(bb.1.uuid.clone()))
                    .unwrap(),
            )?;
        }

        Ok(())
    }

    pub fn merge<'a>(&mut self, uu: UUID, frame: &Frame<'a>) -> Result<()> {
        Self::merge_impl(&*self.db, uu, frame)?;
        Ok(())
    }

    pub fn get<'a>(&self, obj: UUID, ty: UUID) -> Result<Frame<'a>> {
        Self::get_impl(&self.db, obj, ty)
    }

    pub fn persist_to(&self, path: &Path) -> Result<()> {
        use rocksdb::IteratorMode;

        let mut fd = File::create(path)?;

        for (_, frame) in self.db.iterator(IteratorMode::Start) {
            fd.write_all(&*frame)?;
        }

        Ok(())
    }

    pub(crate) fn merge_impl<'a>(
        db: &rocksdb::DB, uu: UUID, frame: &Frame<'a>,
    ) -> Result<()> {
        db.merge(&Self::uuid_as_key(uu)[..], frame.body().as_bytes())?;
        Ok(())
    }

    pub(crate) fn get_impl<'a>(
        db: &rocksdb::DB, obj: UUID, ty: UUID,
    ) -> Result<Frame<'a>> {
        let key = Self::uuid_as_key(obj);
        match db.get(&key[..]) {
            Ok(Some(ref vec)) => {
                let frame = Frame::parse(
                    vec.to_utf8()
                        .ok_or(Error::from("frame not UTF-8"))?
                        .to_string(),
                );

                match frame.peek() {
                    Some(op) if ty == op.ty => Ok(frame),
                    Some(op) => {
                        Err(Error::from(format!(
                            "object has wrong type: expected {}, got {}",
                            ty, op.ty
                        ))
                        .into())
                    }
                    None => {
                        Err(Error::from(format!(
                            "object {} has an empty state frame",
                            obj
                        ))
                        .into())
                    }
                }
            }

            Ok(None) => {
                Err(Error::from(format!("no object {} of type {}", obj, ty))
                    .into())
            }

            Err(e) => Err(e.into()),
        }
    }

    pub(crate) fn new_database() -> Result<(rocksdb::DB, TempDir)> {
        let tempdir = TempDir::new("p8n-sync-db")?;
        let mut opts = rocksdb::Options::default();
        opts.create_if_missing(true);
        opts.set_merge_operator(
            "RON reduce operator operator",
            reduce_operation,
            None,
        );

        let db = rocksdb::DB::open(&opts, tempdir.path())?;

        Ok((db, tempdir))
    }

    fn setup(&mut self) -> Result<()> {
        use std::mem::swap;

        let mut regs = Vec::default();

        swap(&mut regs, &mut self.regions);
        for reg in regs.iter() {
            serialize_region(reg, self)?;

            let frm = Set::new(self.region_set);
            let frm =
                Set::insert(&frm, Atom::UUID(reg.uuid().clone())).unwrap();

            self.region_refs.insert(*reg.uuid(), *reg.uuid());
            self.merge(self.region_set, &frm)?;
        }
        swap(&mut regs, &mut self.regions);

        // db
        let db_frm = LWW::new(*DB_UUID);
        self.merge(*DB_UUID, &db_frm)?;

        // db name
        let db_name =
            LWW::set(&db_frm, *NAME_UUID, Atom::String(self.name.to_string()))
                .unwrap();
        self.merge(*DB_UUID, &db_name)?;

        // functions field
        let fns = LWW::set(&db_frm, *FNS_UUID, Atom::UUID(self.function_set))
            .unwrap();
        self.merge(*DB_UUID, &fns)?;

        // regions field
        let rgs =
            LWW::set(&db_frm, *REGS_UUID, Atom::UUID(self.region_set)).unwrap();
        self.merge(*DB_UUID, &rgs)?;

        // markers field
        let mrks =
            LWW::set(&db_frm, *MARKERS_UUID, Atom::UUID(self.marker_set))
                .unwrap();
        self.merge(*DB_UUID, &mrks)?;

        Ok(())
    }

    fn uuid_as_key(uu: UUID) -> [u8; 16] {
        match uu {
            UUID::Name { name, scope } => {
                [
                    (name >> 56) as u8,
                    (name >> 48) as u8,
                    (name >> 40) as u8,
                    (name >> 32) as u8,
                    (name >> 24) as u8,
                    (name >> 16) as u8,
                    (name >> 8) as u8,
                    (name >> 0) as u8,
                    (scope >> 56) as u8,
                    (scope >> 48) as u8,
                    (scope >> 40) as u8,
                    (scope >> 32) as u8,
                    (scope >> 24) as u8,
                    (scope >> 16) as u8,
                    (scope >> 8) as u8,
                    (scope >> 0) as u8,
                ]
            }
            UUID::Number { value1, value2 } => {
                [
                    (value1 >> 56) as u8,
                    (value1 >> 48) as u8,
                    (value1 >> 40) as u8,
                    (value1 >> 32) as u8,
                    (value1 >> 24) as u8,
                    (value1 >> 16) as u8,
                    (value1 >> 8) as u8,
                    (value1 >> 0) as u8,
                    (value2 >> 56) as u8,
                    (value2 >> 48) as u8,
                    (value2 >> 40) as u8,
                    (value2 >> 32) as u8,
                    (value2 >> 24) as u8,
                    (value2 >> 16) as u8,
                    (value2 >> 8) as u8,
                    (value2 >> 0) as u8,
                ]
            }
            UUID::Derived { timestamp, origin } => {
                [
                    (timestamp >> 56) as u8,
                    (timestamp >> 48) as u8,
                    (timestamp >> 40) as u8,
                    (timestamp >> 32) as u8,
                    (timestamp >> 24) as u8,
                    (timestamp >> 16) as u8,
                    (timestamp >> 8) as u8,
                    (timestamp >> 0) as u8,
                    (origin >> 56) as u8,
                    (origin >> 48) as u8,
                    (origin >> 40) as u8,
                    (origin >> 32) as u8,
                    (origin >> 24) as u8,
                    (origin >> 16) as u8,
                    (origin >> 8) as u8,
                    (origin >> 0) as u8,
                ]
            }
            UUID::Event { timestamp, origin } => {
                [
                    (timestamp >> 56) as u8,
                    (timestamp >> 48) as u8,
                    (timestamp >> 40) as u8,
                    (timestamp >> 32) as u8,
                    (timestamp >> 24) as u8,
                    (timestamp >> 16) as u8,
                    (timestamp >> 8) as u8,
                    (timestamp >> 0) as u8,
                    (origin >> 56) as u8,
                    (origin >> 48) as u8,
                    (origin >> 40) as u8,
                    (origin >> 32) as u8,
                    (origin >> 24) as u8,
                    (origin >> 16) as u8,
                    (origin >> 8) as u8,
                    (origin >> 0) as u8,
                ]
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tempfile::NamedTempFile;
    use types::TestArch;

    #[test]
    fn empty() {
        let tmp = NamedTempFile::new().unwrap();
        let r1 = Region::undefined("l1", 6, None);

        {
            let db = Database::create("db1", vec![r1]).unwrap();
            db.persist_to(tmp.path().into()).unwrap();
        }

        let (db, fns) = Database::open_file(tmp.path()).unwrap();
        let regs = db.regions();

        assert_eq!(db.name(), "db1");
        assert_eq!(regs.len(), 1);
        assert_eq!(regs[0].name(), "l1");
        assert!(fns.wait().next().is_none());
    }

    #[test]
    fn functions() {
        let tmp = NamedTempFile::new().unwrap();
        let uu1 = UUID::now();
        let uu2 = UUID::now();

        {
            let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3RMi1Cfi0Bf18Aii3J22Aii3Ms3R"
                .to_vec();
            let reg = Region::from_buf("l1", 16, data, 0, None);
            let func1 = Function::new::<TestArch>((), 0, &reg, uu1).unwrap();
            let func2 = Function::new::<TestArch>((), 26, &reg, uu2).unwrap();
            let mut db = Database::create("db1", vec![reg]).unwrap();

            db.update_function(&func1).unwrap();
            db.update_function(&func2).unwrap();
            db.persist_to(tmp.path().into()).unwrap();
        }

        let (db, fns) = Database::open_file(tmp.path().into()).unwrap();
        let regs = db.regions();

        assert_eq!(db.name(), "db1");
        assert_eq!(regs.len(), 1);
        assert_eq!(regs[0].name(), "l1");

        let mut fns = fns.wait().map(|f| f.unwrap()).collect::<Vec<_>>();
        fns.sort_by_key(|f| f.entry_address());

        assert_eq!(fns.len(), 2);
        assert_eq!(fns[0].uuid(), &uu1);
        assert_eq!(fns[1].uuid(), &uu2);
    }

    #[test]
    fn function_update() {
        let tmp = NamedTempFile::new().unwrap();
        let uu1 = UUID::now();

        {
            let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3RMi1Cfi0Bf18Aii3J22Aii3Ms3R"
                .to_vec();
            let reg = Region::from_buf("l1", 16, data, 0, None);
            let mut func1 =
                Function::new::<TestArch>((), 0, &reg, uu1).unwrap();
            let mut db = Database::create("db1", vec![reg]).unwrap();
            db.update_function(&func1).unwrap();
            func1.name = "lalalalala".into();
            db.update_function(&func1).unwrap();
            db.persist_to(tmp.path().into()).unwrap();
        }

        let (db, fns) = Database::open_file(tmp.path()).unwrap();
        let regs = db.regions();

        assert_eq!(db.name(), "db1");
        assert_eq!(regs.len(), 1);
        assert_eq!(regs[0].name(), "l1");

        let mut fns = fns.wait().map(|f| f.unwrap()).collect::<Vec<_>>();
        fns.sort_by_key(|f| f.entry_address());

        assert_eq!(fns.len(), 1);
        assert_eq!(fns[0].uuid(), &uu1);
        assert_eq!(fns[0].name, "lalalalala");
    }

    #[test]
    fn delete_function() {
        let tmp = NamedTempFile::new().unwrap();
        let uu1 = UUID::now();
        let uu2 = UUID::now();

        {
            let data = b"Mi1Cfi0Bf18Aii3J22Aii3Ms3RMi1Cfi0Bf18Aii3J22Aii3Ms3R"
                .to_vec();
            let reg = Region::from_buf("l1", 16, data, 0, None);
            let func1 = Function::new::<TestArch>((), 0, &reg, uu1).unwrap();
            let func2 = Function::new::<TestArch>((), 26, &reg, uu2).unwrap();
            let mut db = Database::create("db1", vec![reg]).unwrap();

            db.update_function(&func1).unwrap();
            db.update_function(&func2).unwrap();
            db.remove_function(&func2).unwrap();
            db.persist_to(tmp.path().into()).unwrap();
        }

        let (db, fns) = Database::open_file(tmp.path()).unwrap();
        let regs = db.regions();

        assert_eq!(db.name(), "db1");
        assert_eq!(regs.len(), 1);
        assert_eq!(regs[0].name(), "l1");

        let fns = fns.wait().map(|f| f.unwrap()).collect::<Vec<_>>();

        assert_eq!(fns.len(), 1);
        assert_eq!(fns[0].uuid(), &uu1);
    }

    #[test]
    fn append() {
        let tmp = NamedTempFile::new().unwrap();
        let uu1 = UUID::now();
        let uu2 = UUID::now();
        let data =
            b"Mi1Cfi0Bf18Aii3J22Aii3Ms3RMi1Cfi0Bf18Aii3J22Aii3Ms3R".to_vec();
        let reg = Region::from_buf("l1", 16, data, 0, None);
        let func1 = Function::new::<TestArch>((), 0, &reg, uu1).unwrap();
        let func2 = Function::new::<TestArch>((), 26, &reg, uu2).unwrap();

        {
            let mut db = Database::create("db1", vec![reg]).unwrap();

            db.update_function(&func1).unwrap();
            db.update_function(&func2).unwrap();
            db.persist_to(tmp.path().into()).unwrap();
        }

        {
            let (mut db, _) = Database::open_file(tmp.path()).unwrap();

            db.remove_function(&func2).unwrap();
            db.persist_to(tmp.path().into()).unwrap();
        }

        let (db, fns) = Database::open_file(tmp.path()).unwrap();
        let regs = db.regions();

        assert_eq!(db.name(), "db1");
        assert_eq!(regs.len(), 1);
        assert_eq!(regs[0].name(), "l1");

        let fns = fns.wait().map(|f| f.unwrap()).collect::<Vec<_>>();

        assert_eq!(fns.len(), 1);
        assert_eq!(fns[0].uuid(), &uu1);
    }
}

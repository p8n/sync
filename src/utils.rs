use ron_crdt::{Atom, UUID};
use std::collections::HashMap;

use crate::{Error, Result};

pub fn int_field<'a>(key: UUID, lww: &HashMap<UUID, Atom>) -> Result<i64> {
    lww.get(&key)
        .and_then(
            |atm| {
                if let &Atom::Integer(i) = atm {
                    Some(i)
                } else {
                    None
                }
            },
        )
        .ok_or(Error::from(format!("no integer field named {}", key)).into())
}

pub fn uuid_field<'a>(key: UUID, lww: &HashMap<UUID, Atom>) -> Result<UUID> {
    lww.get(&key)
        .and_then(|atm| if let &Atom::UUID(uu) = atm { Some(uu) } else { None })
        .ok_or(Error::from(format!("no UUID field named {}", key)).into())
}

pub fn string_field<'a>(
    key: UUID, lww: &'a HashMap<UUID, Atom>,
) -> Result<&'a str> {
    lww.get(&key)
        .and_then(|atm| {
            if let &Atom::String(ref s) = atm {
                Some(s.as_str())
            } else {
                None
            }
        })
        .ok_or(Error::from(format!("no string field named {}", key)).into())
}
